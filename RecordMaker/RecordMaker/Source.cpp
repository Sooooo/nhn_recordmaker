#include <iostream>
#include <string>
#include <set>
#include <ctime>
#include <cstdlib>

using namespace std;

string makeNTNum();
void print();

set<string> NTNumList;	//Number List

int main(int argc, char* argv[]){

	if(argc == 1){
		cout<<"Please Input Option"<<endl;
		return 0;
	}

	int Count = atoi(argv[1]);			//Input Option
	
	while(NTNumList.size() != Count){
		NTNumList.insert(makeNTNum());
	}

	print();
	
	return 0;
}

string makeNTNum(){
	
	srand((unsigned int)time(NULL));
		
	int m = rand() % 100;
	long long int num = (rand()*m) % 100000;
	string retNum = to_string(num);

	while(retNum.length() < 5){
		retNum.insert(0, "0");
	}
		
	retNum.insert(0, "NT");

	return retNum;
}

void print(){
	//information Print

	set<string>::iterator iter;

	srand((unsigned int)time(NULL));
	for(iter = NTNumList.begin(); iter != NTNumList.end(); ++iter){
		
		int Score = rand() % 100;


		cout<<iter->data()<<" "<<Score<<endl;	
	}		
}